package com.playtika.java.training.challenge2.rotaru.gheorgheiulian;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private int noCards;
    private String name;
    private List<PlayerCard> ownedCards;
    private boolean isBingoTime;
    private String winningSchema;

    public Player(String name, int noCards) {
        this.name = name;
        this.noCards = noCards;
        ownedCards = new ArrayList<>();
        this.isBingoTime = false;
        this.winningSchema = "";
    }

    public void setPlayerCards(List<PlayerCard> playerCards) {

    }

    public int getNoCards() {
        return this.noCards;
    }

    public void checkCardsToWin() {

    }

    public boolean hasSaidBingo() {
        return this.isBingoTime;
    }
}
