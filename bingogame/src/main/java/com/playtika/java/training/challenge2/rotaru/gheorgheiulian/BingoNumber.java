package com.playtika.java.training.challenge2.rotaru.gheorgheiulian;

import com.playtika.java.training.challenge2.rotaru.gheorgheiulian.interfaces.Printable;

public class BingoNumber implements Cloneable, Printable {
    private String column;
    private int number;

    public BingoNumber(String column, int number) {
        this.column = column;
        this.number = number;
    }

    public boolean isValidCombination(String column, int number) {
        return true;
    }

    public String getColumn() {
        return column;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public BingoNumber clone() {
        BingoNumber copy = new BingoNumber(this.getColumn(), this.getNumber());
        return copy;
    }

    @Override
    public void print() {
        System.out.println(String.format("Column: %s ; Number: %d ;", this.getColumn(), this.getNumber()));
    }

    @Override
    public String toString() {
        return "BingoNumber{" +
                "column='" + column + '\'' +
                ", number=" + number +
                '}';
    }
}
