package com.playtika.java.training.challenge2.rotaru.gheorgheiulian.exceptions;

public class NoAvailablePlayersException extends Exception {

    public NoAvailablePlayersException(String mesage) {
        System.out.println(mesage);
    }
}
