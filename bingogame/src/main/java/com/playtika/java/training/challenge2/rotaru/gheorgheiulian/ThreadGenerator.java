package com.playtika.java.training.challenge2.rotaru.gheorgheiulian;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

public class ThreadGenerator implements Callable<List<PlayerCard>> {

    private final int lowIndex;
    private final int upIndex;

    public ThreadGenerator(int lowIndex, int upIndex) {
        this.lowIndex = lowIndex;
        this.upIndex = upIndex;
    }

    private List<BingoNumber> generateColumn(int randomNumber, int numberColumn, int numberCells) {
        Random random = new Random(this.upIndex + numberColumn + randomNumber);
        String[] columnLetter = {"B", "I", "N", "G", "O"};

        int[] viz = new int[15];
        for(int k = 0; k < 15; k++) {
            viz[k] = 0;
        }

        List<BingoNumber> generatedColumn = new ArrayList<>();

        for(int k = 0; k < numberCells; k++) {
            int generateNumber = random.nextInt(15);

            while(viz[generateNumber] == 1) {
                generateNumber = random.nextInt(15);
            }

            generatedColumn.add(new BingoNumber(columnLetter[numberColumn], generateNumber + 1 + numberColumn * 15));
            viz[generateNumber] = 1;
        }

        return generatedColumn;
    }

    @Override
    public List<PlayerCard> call() {
        List<PlayerCard> resultThreadGenerator = new ArrayList<>();

        for(int j = lowIndex; j < upIndex; j++) {
            List<BingoNumber> bingoCardNumbers = new ArrayList<>();
            bingoCardNumbers.addAll(generateColumn(j, 0, 5));
            bingoCardNumbers.addAll(generateColumn(j, 1, 5));
            bingoCardNumbers.addAll(generateColumn(j, 2, 4));
            bingoCardNumbers.addAll(generateColumn(j, 3, 5));
            bingoCardNumbers.addAll(generateColumn(j, 4, 5));

            resultThreadGenerator.add(new PlayerCard(bingoCardNumbers));
        }

        return resultThreadGenerator;
    }
}
