package com.playtika.java.training.challenge2.rotaru.gheorgheiulian.interfaces;

public interface Printable {
    public void print();
}
