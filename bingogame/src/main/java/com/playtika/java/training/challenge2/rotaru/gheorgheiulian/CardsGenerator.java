package com.playtika.java.training.challenge2.rotaru.gheorgheiulian;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class CardsGenerator {
    private final int noThreads;
    private final int noCards;
    private boolean hasFinishedGeneratingData;

    public CardsGenerator(int noThreads, int noCards) {
        this.noThreads = noThreads;
        this.noCards = noCards;
        this.hasFinishedGeneratingData = false;
    }

    public List<PlayerCard> getInitialCards() {

        List<Callable<List<PlayerCard>>> threads = new ArrayList<>();

        int sequenceLength = noCards / noThreads;
        for(int i = 0; i < noThreads; i++) {
            threads.add(new ThreadGenerator(i * sequenceLength, (i + 1) * sequenceLength));
        }

        ExecutorService serviceCards = Executors.newFixedThreadPool(noThreads);
        List<Future<List<PlayerCard>>> resultThreads = new ArrayList<>();

        try {
            resultThreads = serviceCards.invokeAll(threads);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        List<PlayerCard> playerCards = new ArrayList<>();

        for(Future<List<PlayerCard>> playerCardsOnThread : resultThreads) {
            List<PlayerCard> resultThread = new ArrayList<>();
            try {
                resultThread = playerCardsOnThread.get();
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            } catch (ExecutionException exception) {
                exception.printStackTrace();
            }

            playerCards.addAll(resultThread);
        }

        serviceCards.shutdown();
        try {
            if(!serviceCards.awaitTermination(3000, TimeUnit.MILLISECONDS)) {
                serviceCards.shutdown();
            }
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        this.hasFinishedGeneratingData = true;
        return playerCards;
    }

    public boolean hasFinished() {
        return this.hasFinishedGeneratingData;
    }
}
