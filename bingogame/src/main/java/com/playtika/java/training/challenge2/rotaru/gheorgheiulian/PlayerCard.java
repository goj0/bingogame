package com.playtika.java.training.challenge2.rotaru.gheorgheiulian;

import com.playtika.java.training.challenge2.rotaru.gheorgheiulian.interfaces.Printable;

import javax.swing.plaf.IconUIResource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlayerCard implements Printable {
    public final String UID;
    private List<BingoNumber> numbers = new ArrayList<>();
    private BingoNumber[][] matrixBingoGame = new BingoNumber[5][5];

    public PlayerCard(List<BingoNumber> numbers) {
        this.UID = UUID.randomUUID().toString();

        for(BingoNumber bingoNumber : numbers) {
            this.numbers.add(bingoNumber);
        }

        for(int i = 0; i < 12; i++) {
            matrixBingoGame[i%5][i/5] = new BingoNumber(numbers.get(i).getColumn(), numbers.get(i).getNumber());
        }

        matrixBingoGame[2][2] = new BingoNumber("N", 0);

        for(int i = 12; i < 24; i++) {
            matrixBingoGame[(i+1)%5][(i+1)/5] = new BingoNumber(numbers.get(i).getColumn(), numbers.get(i).getNumber());
        }
    }

    @Override
    public void print() {

    }

    @Override
    public String toString() {
        String result =  "UID=" + UID + "\n" +
                "Numbers size: " + numbers.size() + "\n" +
                "B  I  N  G  O\n";
        for(int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                result += matrixBingoGame[i][j].getNumber() + " ";
            }
            result += "\n";
        }

        return result;
    }
}
