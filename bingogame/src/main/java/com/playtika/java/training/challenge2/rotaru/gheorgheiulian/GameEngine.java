package com.playtika.java.training.challenge2.rotaru.gheorgheiulian;

import com.playtika.java.training.challenge2.rotaru.gheorgheiulian.exceptions.NoGameSettingsException;

import java.io.IOException;
import java.util.List;

public class GameEngine {
    public static void main(String[] args) throws IOException {
        GameSettings gameSettings = new GameSettings("settings.txt");
        CardsGenerator cardsGenerator = new CardsGenerator(2, 4); //gameSettings.NO_CARDS);
        List<PlayerCard> cards = cardsGenerator.getInitialCards();
        System.out.println(cards);
    }
}
