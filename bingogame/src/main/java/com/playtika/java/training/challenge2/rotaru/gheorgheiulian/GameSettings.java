package com.playtika.java.training.challenge2.rotaru.gheorgheiulian;

import com.playtika.java.training.challenge2.rotaru.gheorgheiulian.exceptions.NoAvailableForbiddenCombination;
import com.playtika.java.training.challenge2.rotaru.gheorgheiulian.exceptions.NoAvailablePlayersException;
import com.playtika.java.training.challenge2.rotaru.gheorgheiulian.exceptions.NoGameSettingsException;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class GameSettings implements Cloneable {
    public static int NO_CARDS;
    private int noPlayers;
    private List<String> playerNames = new ArrayList<>();
    private List<List<BingoNumber>> forbiddenCombinations = new ArrayList<>();

    public GameSettings(String settingsFileName) throws IOException {
        File file = new File(settingsFileName);

        if(!file.exists()) {
            throw new NoGameSettingsException("Could not find file");
        }

        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        line = bufferedReader.readLine();
        if(line == null) {
            throw  new NoGameSettingsException("Number of cards to be generated not found");
        }

        this.NO_CARDS = Integer.parseInt(line);

        line = bufferedReader.readLine();
        if(line == null) {
            throw  new NoGameSettingsException("Number of players not found");
        }

        this.noPlayers = Integer.parseInt(line);

        while((line = bufferedReader.readLine()) != null) {
            if(line.contains(",")) {
                break;
            }

            this.playerNames.add(line);
        }

        while((line = bufferedReader.readLine()) != null) {
            this.forbiddenCombinations.add(createforbiddenCombinationBingoNumber(line));
        }
    }

    private List<BingoNumber> createforbiddenCombinationBingoNumber(String line) {
        String[] values = line.split(",");

        List<BingoNumber> column = new ArrayList<>();

        for(int i = 1; i < 6; i++) {
            column.add(new BingoNumber(values[0], Integer.parseInt(values[i])));
        }

        return column;
    }

    private GameSettings(int noPlayers, List<String> playerNames, List<List<BingoNumber>> forbiddenCombinations) {
        this.noPlayers = noPlayers;

        for(String playerName : playerNames) {
            this.playerNames.add(playerName);
        }

        for(List<BingoNumber> column : forbiddenCombinations) {
            List<BingoNumber> newForbiddenCombination = new ArrayList<>();

            for(BingoNumber bingoNumber : column) {
                newForbiddenCombination.add(bingoNumber);
            }
            this.forbiddenCombinations.add(newForbiddenCombination);
        }
    }

    public int getNoPlayers() {
        return noPlayers;
    }

    public String getPlayerName(int index) throws NoAvailablePlayersException {
        if(index >= playerNames.size()) {
            throw new NoAvailablePlayersException("There are not that many players");
        }

        return playerNames.get(index);
    }

    public int getNoForbiddenCombinations() {
        return forbiddenCombinations.size();
    }

    public List<BingoNumber> getForbiddenCombination(int index) throws NoAvailableForbiddenCombination {
        if(index >= forbiddenCombinations.size()) {
            throw new NoAvailableForbiddenCombination("There are not that many forbidden combination");
        }

        return forbiddenCombinations.get(index);
    }

    @Override
    public GameSettings clone() {
        return new GameSettings(this.getNoPlayers(), this.playerNames, this.forbiddenCombinations);
    }

    @Override
    public String toString() {
        return "GameSettings{" +
                "noPlayers=" + noPlayers +
                ", playerNames=" + playerNames +
                ", forbiddenCombinations=" + forbiddenCombinations +
                '}';
    }
}
