package com.playtika.java.training.challenge2.rotaru.gheorgheiulian.exceptions;

import java.io.IOException;

public class NoGameSettingsException extends IOException {

    public NoGameSettingsException(String mesage) {
        System.out.println(mesage);
    }
}
