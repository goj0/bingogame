package com.playtika.java.training.challenge2.rotaru.gheorgheiulian;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Caller {
    private char lastNumberColumn;
    private int lastNumberNumber;
    private AtomicInteger noRounds;
    private List<BingoNumber> previousCombinations;
    private boolean isGameFinished;

    public Caller() {
        this.lastNumberColumn = ' ';
        this.lastNumberNumber = 0;
        this.noRounds.set(0);
        previousCombinations = new ArrayList<>();
        this.isGameFinished = false;
    }

    public boolean wasPicked(BingoNumber bingoNumber) {
        for(BingoNumber bingoNumberSelected : previousCombinations) {
            if(bingoNumberSelected.equals(bingoNumber)) {
                return true;
            }
        }
        this.previousCombinations.add(bingoNumber);

        return false;
    }

    public char getLastColumn() {
        return this.lastNumberColumn;
    }

    public int getLastNumber() {
        return this.lastNumberNumber;
    }

    public int getNoOfPlayedNumbers() {
        return previousCombinations.size();
    }

    public List<BingoNumber> getPreviousCombinations() {
        List<BingoNumber> previousCombination = new ArrayList<>();
        for(BingoNumber bingoNumber : previousCombinations) {
            previousCombination.add(bingoNumber);
        }

        return previousCombination;
    }

    public boolean checkGameStatus() {
        return this.isGameFinished;
    }
}
